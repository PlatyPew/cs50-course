#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>

const int PENNY = 1;
const int NICKEL = 5;
const int DIME = 10;
const int QUARTER = 25;

float getFloat(const char *format, ...) {
    char* buffer = NULL;
    size_t size = 0;
    size_t capacity = 0;

    if (format != NULL) {
        va_list ap;
        va_start(ap, format);
        vprintf(format, ap);
        va_end(ap);
    }

    int c;

    while ((c = fgetc(stdin)) != '\n' && c != EOF) {
        if (size + 1 > capacity) {
            capacity++;
        }
        buffer = realloc(buffer, capacity);

        buffer[size++] = c;
    }

    if (size == 0 && c == EOF) {
        return 0;
    }

    buffer = realloc(buffer, size + 1);
    buffer[size] = '\0';

    return strtof(buffer, NULL);
}

int coins(int cents) {
    int count;
    int sum = 0;

    count = cents / QUARTER;
    for (int i = 0; i < count; i++) {
        cents -= QUARTER;
        sum++;
    }

    count = cents / DIME;
    for (int i = 0; i < count; i++) {
        cents -= DIME;
        sum++;
    }

    count = cents / NICKEL;
    for (int i = 0; i < count; i++) {
        cents -= NICKEL;
        sum++;
    }

    count = cents / PENNY;
    for (int i = 0; i < count; i++) {
        cents -= PENNY;
        sum++;
    }

    return sum;
}

int main() {
    int cents;
    do {
        cents = round(getFloat("Change owed: ") * 100);
    } while (cents <= 0);

    printf("%d\n", coins(cents));

    return 0;
}
