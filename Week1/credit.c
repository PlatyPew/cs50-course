#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

const int AMEX = 3;
const int VISA = 4;
const int MASTERCARD = 5;

char* getString(const char *format, ...) {
    char* buffer = NULL;
    size_t size = 0;
    size_t capacity = 0;

    if (format != NULL) {
        va_list ap;
        va_start(ap, format);
        vprintf(format, ap);
        va_end(ap);
    }

    int c;

    while ((c = fgetc(stdin)) != '\n' && c != EOF) {
        if (size + 1 > capacity) {
            capacity++;
        }
        buffer = realloc(buffer, capacity);

        buffer[size++] = c;
    }

    if (size == 0 && c == EOF) {
        return NULL;
    }

    buffer = realloc(buffer, size + 1);
    buffer[size] = '\0';

    return buffer;
}

char* getCardType(char* number) {
    int startNum = *number - '0';
    if (startNum == AMEX) {
        return "AMEX";
    } else if (startNum == VISA) {
        return "VISA";
    } else if (startNum == MASTERCARD) {
        return "MASTERCARD";
    } else {
        return NULL;
    }

}

int luhnAlgo(char* number) {
    int sum = 0;
    int product;
    for (int i = strlen(number) - 1; i >= 0; i -= 2) {
        product = (*(number + i - 1) - '0') * 2;
        if (product / 10 != 0) {
            sum += 1 + (product % 10);
        } else {
            sum += product;
        }
    }

    for (int i = strlen(number) - 1; i >= 0; i -=2) {
        sum += *(number + i) - '0';
    }

    if (sum % 10 == 0) {
        return 1;
    } else {
        return 0;
    }
}

int main() {
    char* number;
    do {
        number = getString("Number: ");
    } while (atol(number) <= 0);

    if (strlen(number) < 13 || strlen(number) > 16) {
        puts("INVALID");
        return 0;
    }

    char* cardType = getCardType(number);
    if (cardType == NULL) {
        puts("INVALID");
        return 0;
    }

    if (luhnAlgo(number)) {
        puts(cardType);
    } else {
        puts("INVALID");
        return 0;
    }

    return 0;
}
