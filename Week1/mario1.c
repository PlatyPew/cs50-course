#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

char* getString(const char *format, ...) {
    char* buffer = NULL;
    size_t size = 0;
    size_t capacity = 0;

    if (format != NULL) {
        va_list ap;
        va_start(ap, format);
        vprintf(format, ap);
        va_end(ap);
    }

    int c;

    while ((c = fgetc(stdin)) != '\n' && c != EOF) {
        if (size + 1 > capacity) {
            capacity++;
        }
        buffer = realloc(buffer, capacity);

        buffer[size++] = c;
    }

    if (size == 0 && c == EOF) {
        return NULL;
    }

    buffer = realloc(buffer, size + 1);
    buffer[size] = '\0';

    return buffer;
}

void marioPipe(int height) {
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < height - i - 1; j++) {
            printf(" ");
        }
        for (int k = 0; k < i + 1; k++) {
            printf("#");
        }
        printf("\n");
    }
}

int main() {
    int height;
    do {
        height = atoi(getString("Height: "));
    } while (height < 1 || height > 8);

    marioPipe(height);
    return 0;
}
