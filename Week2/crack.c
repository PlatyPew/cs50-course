#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

char* crack(char* hash) {
    char *test = (char*)malloc(sizeof(char) * 6);

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 26; j++) {
            test[i] = j + 'A';
            test[i + 1] = '\0';
            puts(test);
            if (strcmp(hash, test) == 0) {
                return test;
            }
        }

        for (int k = 0; k < 26; k++) {
            test[i] = k + 'a';
            test[i + 1] = '\0';
            puts(test);
            if (strcmp(hash, test) == 0) {
                return test;
            }
        }
    }
    return NULL;
}

int main(int argc, char* argv[]) {
    if (argc != 2) {
        printf("Usage: %s hash\n", argv[0]);
    }

    char *hash = argv[1];
    crack(hash);
    return 0;
}
