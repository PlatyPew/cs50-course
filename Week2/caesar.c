#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

char* getString(const char *format, ...) {
    char* buffer = NULL;
    size_t size = 0;
    size_t capacity = 0;

    if (format != NULL) {
        va_list ap;
        va_start(ap, format);
        vprintf(format, ap);
        va_end(ap);
    }

    int c;

    while ((c = fgetc(stdin)) != '\n' && c != EOF) {
        if (size + 1 > capacity) {
            capacity++;
        }
        buffer = realloc(buffer, capacity);

        buffer[size++] = c;
    }

    if (size == 0 && c == EOF) {
        return NULL;
    }

    buffer = realloc(buffer, size + 1);
    buffer[size] = '\0';

    return buffer;
}

int usage(char* programName) {
    printf("Usage: %s key\n", programName);
    exit(1);
}

char* shift(int key, char* plaintext) {
    char* ciphertext = (char*)malloc(sizeof(char) * strlen(plaintext) + 1);
    for (int i = 0; i < strlen(plaintext); i++) {
        if (plaintext[i] >= 'A' && plaintext[i] <= 'Z') {
            ciphertext[i] = (plaintext[i] - 'A' + key) % 26 + 'A';
        } else if (plaintext[i] >= 'a' && plaintext[i] <= 'z') {
            ciphertext[i] = (plaintext[i] - 'a' + key) % 26 + 'a';
        } else {
            ciphertext[i] = plaintext[i];
        }
    }

    ciphertext[strlen(plaintext)] = '\0';
    return ciphertext;
}

int main(int argc, char* argv[]) {
    if (argc != 2) {
        usage(argv[0]);
    }

    int key = atoi(argv[1]);

    if (key == 0) {
        usage(argv[0]);
    }

    char* plaintext = getString("plaintext: ");
    printf("ciphertext: %s\n", shift(key, plaintext));

    return 0;
}
