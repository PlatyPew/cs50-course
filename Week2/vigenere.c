#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

char* getString(const char *format, ...) {
    char* buffer = NULL;
    size_t capacity = 0;

    if (format != NULL) {
        va_list ap;
        va_start(ap, format);
        vprintf(format, ap);
        va_end(ap);
    }

    int c;

    while ((c = fgetc(stdin)) != '\n' && c != EOF) {
        buffer = realloc(buffer, capacity + 1);
        buffer[capacity++] = c;
    }

    if (capacity == 0 && c == EOF) {
        return NULL;
    }

    buffer = realloc(buffer, capacity + 1);
    buffer[capacity] = '\0';

    return buffer;
}

int usage(char* programName) {
    printf("Usage: %s key\n", programName);
    exit(1);
}

char* encrypt(char* key, char* plaintext) {
    char* ciphertext = (char*)malloc(sizeof(char) * strlen(plaintext) + 1);

    int count = 0;
    for (int i = 0; i < strlen(plaintext); i++) {
        if (plaintext[i] >= 'A' && plaintext[i] <= 'Z') {
            ciphertext[i] = (plaintext[i] - 'A' + key[count % strlen(key)] - 'a') % 26 + 'A';
            count++;
        } else if (plaintext[i] >= 'a' && plaintext[i] <= 'z') {
            ciphertext[i] = (plaintext[i] - 'a' + key[count % strlen(key)] - 'a') % 26 + 'a';
            count++;
        } else {
            ciphertext[i] = plaintext[i];
        }
    }

    ciphertext[strlen(plaintext)] = '\0';

    return ciphertext;
}

int main(int argc, char* argv[]) {
    if (argc != 2) {
        usage(argv[0]);
    }

    for (int i = 0; i < strlen(argv[1]); i++) {
        if (argv[1][i] < 'a' || argv[1][i] > 'z') {
            usage(argv[0]);
        }
    }

    if (strlen(argv[1]) == 0) usage(argv[0]);

    char* plaintext = getString("plaintext: ");
    if (plaintext == NULL) usage(argv[0]);

    char* ciphertext = encrypt(argv[1], plaintext);

    printf("ciphertext: %s\n", ciphertext);
    free(ciphertext);

    return 0;
}
